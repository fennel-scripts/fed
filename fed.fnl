#!/usr/bin/env fennel
;;; import
;;;; from luarocks
(local rl (require :readline))
(local gum (require :bubblegum))

;;;;; set options for readline
(local rlopts {:keeplines 5000 :histfile "~/.fedhist"})
(local filehist "~/.fedhist-files")
(rl.set_options rlopts)
(rl.set_readline_name "fed")

;;;; localy


(local file-lib		(require :fed.file))
(local fringe		(require :fed.fringe))
(local prompts		(require :fed.prompts))
(local pl			(require :fed.plates))
(local cmds			(require :fed.cmds))
(local {: line : lines} (require :fed.lines))



;;; global objects
(local fed {:continue true})
(local repl {:continue true})
(local userdata {})
;;; standard-ish functions
(local tcat table.concat)
(lambda tprint [tbl ?sep]  (print (tcat tbl (or ?sep " "))))
(lambda split [input ?sep]
  (let [sep (or ?sep "%s")]
	(let [result {}
		  matches (string.gmatch input (.. "([^" sep "]+)"))]
	  (each [str matches] (table.insert result str))
	  result)))
;;; ,,,







;;; readline wrapper
(lambda read [{:prompt ?prompt}]
  (let [input (gum.input {:header prompts.default :prompt (or ?prompt prompts.default)})]	(print "input" input) input)
  ;;(let [input (rl.readline (or ?prompt (prompts.default)))]	(rl.save_history) input)
  ;;(io.write (or ?prompt (prompts.default)))
  ;;(io.read)
  )



;;; repl
(lambda repl.read [file] (split (read {:prompt (prompts.file-info file)}) " "))
(lambda repl.loop [file stack] (while (cmds.eval (repl.read file) file stack)))






;;; setup
(lambda setup* []
  ;;(if ?filename)
  (lambda new-file [filename] (file-lib.init {:name filename}))
  (lambda read-filename []
	(rl.set_options {:histfile filehist})
	(local filename (read {:prompt (prompts.read-file-name)}))
	(rl.save_history)
	(rl.set_options rlopts)
	filename
	)
  ;;(lambda read-filename [] (read {:prompt (prompts.read-file-name)}))
	(let [cmd (split (read {:prompt (prompts.default)}) " ")]
	  (var file {})
	  (match cmd
		(where (or [:open filename] [:edit filename]))
		(set file (new-file filename))
		(where (or [:open ] [:edit]))
		(set file (new-file (read-filename)))
		[:q] "exit"
		)
	  file
	  ))

(lambda setup []
  (var file (file-lib.dummy))
  (print "setup" "file" file.name)
  file)



;;(lambda execmode.setup [filename])
;;(lambda execmode.loop [cmds])

;;; main
(fn main [args]
  ;;(print file.name)
  (local stack (pl.new :lines))
  (local file (setup))
  (match args
    (where (or [:--exec filename] [:-x filename]))
	(do
      (local commands
             (. (file-lib.init {:name filename})
                :lines))
      ;;(local file (file-lib.init {:name filename}))
      (each [linenum command (ipairs commands)]
        (cmds.eval (split command " ")
                   file stack)))
    [:--eval filename]
	(do
      (local commands (require (.. "./" filename)))
      (each [linenum command (ipairs commands)]
        (cmds.eval command file stack)))
    _ (do
        (repl.loop file stack))))


(main arg)
(rl.save_history)
