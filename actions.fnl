#!/usr/bin/env fennel
[
 [:open "example.txt"]
 [:read]
 [:print :lines]
 ;;[:edit :line 6 "my name is Erik"]
 [:print :sep]
 [:insert :line 7 "this is now the first line"]
 ["insert" "line" "7" "this" "is" "now" "the" "first" "line"]
 [:print :lines]
 [:w]
 ]
