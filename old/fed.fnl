#!/usr/bin/env fennel

(local rl (require :readline))
(local setopt rl.set_options)
(local set-rl-name rl.set_readline_name)
(local flib (require :file))
(local fringe (require :fringe))
(local prompts (require :prompts))


(local tcat table.concat)
(lambda tprint [tbl ?sep]  (print (tcat tbl (or ?sep " "))))
(var continue true)
(local {: line : lines} (require :lines))
(setopt {:keeplines 200 :histfile "~/.fedhist"})
(set-rl-name "fed")



(lambda split [input ?sep]
  (let [sep (or ?sep "%s")]
	(let [result {}
		  matches (string.gmatch input (.. "([^" sep "]+)"))]
	  (each [str matches] (table.insert result str))
	  result)))

(lambda read [{:prompt ?prompt}] (split (rl.readline (or ?prompt prompts.file-info)) " "))

(lambda syscmds [cmd file]
  (case cmd
	(where (or [:q]	[:quit] [:x] [:exit]))
	(set continue false)
	(where (or [:w]	[:write]))
	(file:write!)
	)
  )

(lambda eval [cmd file]
  (case cmd
	[:lines] (lines.print file)
	[:line linum] (line.print file (tonumber linum))
	[:range from to] (lines.print-range file {:from (tonumber from) :to (tonumber to)}) 
	[:set linum]
	(let [linum (tonumber linum)]
	  (line.print file linum)
	  (let [newcontent (rl.readline (prompts.read-line))]
		(line.set! file linum newcontent)))
	[:set linum & to] (line.set! file (tonumber linum) to)
	_ (syscmds cmd file)
	))

(lambda loop [file]
  (while continue
	(local cmd (read {:prompt (prompts.default file.name)}))
	(eval cmd file)))

(λ setup [])



(lambda main [{:filename filename}]
  (local file (flib.init {:name filename}))
  ;;(lines-print file)
  (loop file)
  (rl.save_history)
  )

(local args {:filename (. arg 1)})
(main args)
