#!/usr/bin/env fennel


(local fennel (require :fennel))
(local gio (require :lib.gio.gio))
(local mytbl {:a 1 :b 2 :c 3})
;;(print (fennel.view mytbl))

;;(collect [k v (pairs {:x 10 :y 11 :z 12}) &into mytbl]  (do (print (fennel.view mytbl)) (values k v)))

;;(print (fennel.view mytbl))
;;(local text	["(local msg :hello)" "(print msg)"])

(fn locals [env read on-values on-error scope]
  "Print all locals in repl session scope."
  (on-values [(fennel.view env.___replLocals___)]))

(lambda callfn [env read on-values]
  
  
  )


;;(fennel.eval (. text 1))
;;(fennel.eval (. text 2))
(fennel.repl {:plugins [{:name "testplugin"
						 :versions [:1.3.0]
						 :repl-command-locals locals
						 :call callfn}]})
