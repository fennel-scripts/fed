#!/usr/bin/env fennel


(local pl {})

(lambda pl.push [this plate] (table.insert this.stack plate))
(lambda pl.pop [this] (table.remove this.stack))
;;(lambda pl.restack [this] (set this.stack (table.sort this.stack)))

(lambda pl.wash [this func]
  (each [index value (ipairs this.stack)]
	(func {: index : value})))



(lambda pl.swap [this from to]
  (let [stack this.stack]
	(let [from-plate (. stack from)
		  to-plate (. stack to)]
	  (tset stack from to-plate)
	  (tset stack to from-plate)
	  )))


(lambda pl.new [name]
  {: name
   :stack []
   :drawer {}
   :push pl.push
   :pop pl.pop
   ;;:restack pl.restack
   :swap pl.swap
   :wash pl.wash
   })


pl



