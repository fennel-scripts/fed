#!/usr/bin/env fennel
;;(local rc (or (require :fed.rc) {}))
(local gum (require :bubblegum))
(local gummy {})
(lambda gummy.delay [time ?unit message]
  (gum.spinner
   {:spinner
	gum.spinners.meter
	;;gum.spinners.points
	:timeout "1s" ;;(string.format "%i%s" (tonumber time) (or (tostring ?unit) "s"))
	:command "sleep 1s" ;;(string.format "sleep %i%s" (tonumber time) (or (tostring ?unit) "s"))
	:align "right"
	:title message
	})
  )


(local f {})

(λ read! [this]
  (let [lines []]
	(with-open [file-in (io.open this.name)]
	  (each [line (file-in:lines)]
		(table.insert lines line)
		))
	(gummy.delay (length lines) :s "reading")
	(set this.lines lines)))

(λ write! [this]
  (with-open [file-out (io.open this.name :w)]
	;;(print "" "writing...")
	(gummy.delay (length this.lines) :s "writing")
	;;(each [linum line (ipairs file.lines)] ;;  (file-out:write line) ;;(print linum line);;)
	(file-out:write (table.concat this.lines "\n"))
	))



(λ f.enforce [{: name : read! : write! : lines : is-file}]
  "will fail if *input* is not a *file*, its rudamentary but it **should** work"
  {: name : read! : write! : lines : is-file})

(lambda f.is-file [file] (. (f.enforce file) :is-file))

(lambda f.new [{:name name}]
  "takes a filename in and returns a *file*"
  (local file
		 {:name name 
		  :read! read!
		  :write! write!
		  :lines []
		  :is-file true
		  })
  file)
(lambda f.dummy []
  {:name "/dev/null"
   :lines []
   :is-file false
   })

(lambda f.create [file]
  (lambda new-file [{: name : lines}]
	{:name name
	 :read! read!
	 :write! write!
	 :lines lines
	 :is-file true
	 })
  (collect [k v (pairs (new-file file)) &into file] k v)
  file)

(lambda f.init [{: name}]
  (local file (f.new {: name}))
  (file:read!)
  (print file.name)
  file)

f
