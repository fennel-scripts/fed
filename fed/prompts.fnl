#!/usr/bin/env fennel

(local prompts {})
(local tcat table.concat)
(lambda prompts.default [_] "the default prompt" "λ: ")
(lambda prompts.file-info [file]
  (tcat [file.name (.. "[" (length file.lines) "]") " λ: "]))
(lambda prompts.read-file-name [_] (tcat ["open a file" " λ: "]))
(lambda prompts.read-line [_] (tcat ["[insert]" " λ: "]))

prompts
