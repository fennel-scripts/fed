#!/usr/bin/env fennel

(local fringe (require :fed.fringe))
(local default-fringe (fringe.new {:a " " :b " " :c " " :d " " :linum " "}))

(local line {})

(lambda line.print [{: lines} linum]
  (let [linum (tonumber linum)]
	(let [f (fringe.new {: linum})]
	  (fringe.print f (. lines linum)))))

(lambda line.set! [file linum newcontent]
  ;;(fringe.print {:a "!" : linum} (type linum))
  
  (let [linum (tonumber linum)]
	(case (type newcontent)
	  (where (or :string :boolean :number))
	  (tset file.lines linum newcontent)
	  :table
	  (tset file.lines linum (table.concat newcontent " ")))))

(local lines {})
(λ lines.print [file] (each [linum _ (ipairs file.lines)] (line.print file linum)))


(lambda lines.print-range [file {:from from :to to}]
  (let [from	(tonumber from)
		to		(tonumber to)]
	(for [linum from (math.min to (length file.lines))]
	  (line.print file linum))))


{: line : lines}
