#!/usr/bin/env fennel
(local bots {})
(local utils {})
(local project (require :project))
(local projname project.name)
(local tcat table.concat)
;;(let [home (os.getenv "HOME") fennel (require :fennel)] (set fennel.path (.. home "/botsdir/lib/" "?.fnl" ";" fennel.path)))
(local cmd (require :botcmd))
(require :botsetup)
(local rc (require :rc))
(local dirs rc.dirs)

(fn pp [o]
  (let [fennel (require :fennel)]
	(print (fennel.view o))))


(fn bots.init [self]
  (let [basedir (.. dirs.stow.src "/" project.name)
		mkdir cmd.mkdir]
	(mkdir [basedir])
	(mkdir [basedir "/" dirs.targets.share])
	(print (.. basedir "/" dirs.targets.share))
	
	(each [_ v (pairs dirs.targets)]
	  (print (.. basedir "/" v))
	  (mkdir [basedir "/" v]))
	(mkdir [basedir "/" dirs.targets.lib project.name])
	))
(set bots.setup bots.init)

(fn bots.build [self]
  (let [install cmd.install
		name project.name
		bin dirs.targets.bin
		lib dirs.targets.lib
		stowdir dirs.stow.src
		projdefs dirs.targets.projdefs
		licensedir dirs.targets.licenses
		libs [:cmds :file :fringe :lines :prompts :plates]
		basedir (.. dirs.stow.src "/" project.name)]
	(install {:src	(tcat [name ".fnl"])	:tgt (tcat [basedir "/" bin  name])})
	(each [_ lname (pairs libs)]  (install {:src	(tcat [name "/" lname ".fnl"]) :tgt	(tcat [basedir "/" lib   name "/" lname ".fnl"])}))
	(install {:src	"project.fnl"  :tgt (tcat [basedir "/" projdefs		name ".fnl"])})
	(install {:src	"LICENSE.org"  :tgt (tcat [basedir "/" licensedir	name ".org"])}))
  (print :done))



(lambda stow-settings [action] {:dir dirs.stow.src :target dirs.stow.tgt :pkgs [projname] : action})
(fn bots.stow	[self] (cmd.stow (stow-settings :stow)))
(fn bots.unstow	[self] (cmd.stow (stow-settings :unstow)))
(fn bots.clean [self]
  (let [name project.name
		;;exec cmd.tprint
		exec cmd.exec
		stowdir dirs.stow.src]
	(self:unstow)
	(exec ["rm " "-r " stowdir "/" name] "")))

(fn bots.install [self]
  (doto self
	;;(: :init)
	(: :build)
	(: :stow)))

(set bots.uninstall bots.unstow)
(fn bots.help [self]
  (print "#" "usage:")
  (print "*" "./bots.fnl <command>")
  (print "#" " commands:")
  (print "*" "init|setup" "preppare by creating missing directories in the filesystem, usually only needed on first run, or after a (ref:clean).")
  (print "*" " (un)stow " "Use GNU stow to stow(link) or unstow(unlink) the program, mostly used internally (ref:install) and for testing.")
  (print "*" "  install " (.. "build the program, copy the files to " dirs.stow.src " and finally, use GNU stow to symlink the files from " dirs.stow.src " to " dirs.stow.tgt "."))
  (print "*" "uninstall " "alias for unstow")
  )

(fn main [arg]
  (match arg
	(where (or :init :setup)) (bots:init)
	:build (bots:build)
	:clean (bots:clean)
	:stow							(utils.stow :stow)
	(where (or :uninstall :unstow)) (utils.stow :unstow)
	:install	(bots:install)
	:help		(bots:help)
	_			(bots:help)))


;;(pp arg)

;;(when (= (. arg 0) "./bots.fnl") (main (. arg 1)))

bots

